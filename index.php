<?php
include('config.php');
include('function.php');
error_reporting(E_ALL);

$name = $author = $isbn = '';
$pdo = new PDO(DB, LOGIN, PW);

$sql = "SELECT * FROM books";

if (!empty($_POST['name']) || !empty($_POST['author']) || !empty($_POST['isbn']))
{// если поле не пустое обрабатываем переданные данные
	$search = array();
	$search['name'] = isset($_POST['name'])?$_POST['name']:'';
	$search['author'] = isset($_POST['author'])?$_POST['author']:'';
	$search['isbn'] = isset($_POST['isbn'])?$_POST['isbn']:'';

	foreach ($search as $key=>$value){
		if (empty($value)) continue;					// если значение пустое - не обрабатываем его
		$search[$key] = clearStr($value); 				// очищаем переданные пользователем данные
		$like[$key] = "{$key} LIKE '%{$search[$key]}%'"; // подготавливаем массив частей запроса
	}
	// собираем в единную строку части нашего SQL-запроса
	$sql .= " WHERE ".implode(" and ", $like);
	// разбиваем массив на переменные для нашей формы
	extract($search);
}
include ('booklist.html');
?>